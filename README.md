# chi-sample

See [go-chi/chi](https://github.com/go-chi/chi).

## How to start

```bash
$ go run main.go 
start server on 3000
```

## How to test

```bash
$ curl -s localhost:3000/
Hello, World!
```

```bash
$ curl -v localhost:3000/user/123
*   Trying 127.0.0.1:3000...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 3000 (#0)
> GET /user/123 HTTP/1.1
> Host: localhost:3000
> User-Agent: curl/7.68.0
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 401 Unauthorized
< Www-Authenticate: Basic realm="my-realm"
< Date: Mon, 21 Nov 2022 10:12:23 GMT
< Content-Length: 0
< 
* Connection #0 to host localhost left intact
```

```bash
$ curl -v --basic -u t2y:secret localhost:3000/user/123
*   Trying 127.0.0.1:3000...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 3000 (#0)
* Server auth using Basic with user 't2y'
> GET /user/123 HTTP/1.1
> Host: localhost:3000
> Authorization: Basic dDJ5OnNlY3JldA==
> User-Agent: curl/7.68.0
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Content-Type: application/json; charset=utf-8
< Date: Mon, 21 Nov 2022 10:12:52 GMT
< Content-Length: 26
<
{"name":"t2y","id":"123"}
* Connection #0 to host localhost left intact
```

