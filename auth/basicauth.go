package auth

import (
	"context"
	"net/http"

	"github.com/go-chi/chi/v5/middleware"
)

func NewStoreUserName() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			username, _, _ := r.BasicAuth()
			req := r.WithContext(context.WithValue(r.Context(), "username", username))
			next.ServeHTTP(w, req)
		})
	}
}

func NewBasicAuth() func(http.Handler) http.Handler {
	realm := "my-realm"
	creds := map[string]string{
		"t2y": "secret",
		"bob": "passwd",
	}
	return middleware.BasicAuth(realm, creds)
}
