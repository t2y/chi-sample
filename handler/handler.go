package handler

import (
	"chi-sample/auth"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

type User struct {
	Name string `json:"name"`
	ID   string `json:"id"`
}

func getUser(w http.ResponseWriter, r *http.Request) {
	data := &User{
		Name: r.Context().Value("username").(string),
		ID:   chi.URLParam(r, "id"),
	}
	render.JSON(w, r, data)
}

func RegsiterUserHandler(m *chi.Mux) {
	m.Route("/user", func(r chi.Router) {
		r.Use(auth.NewBasicAuth())
		r.Use(auth.NewStoreUserName())
		r.Get("/{id}", getUser)
	})
}
